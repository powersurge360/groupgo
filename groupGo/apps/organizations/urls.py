from django.conf.urls import patterns, url
from apps.organizations.views import (
    MapView, AddOrgView, AddCarView, OrganizationListAPIView, OrganizationDetailAPIView,
    CarPoolerListAPIView, CarPoolerDetailAPIView, OrganizationCarPoolerAPIView
)


urlpatterns = patterns(
    '',
    url(r'^$', MapView.as_view(), name='map'),
    url(r'^addOrg$', AddOrgView.as_view(), name='addOrg'),
    url(r'^addCar$', AddCarView.as_view(), name='addCar'),
    url(
        r'^api/organizations$',
        OrganizationListAPIView.as_view(),
        name='api-organizations',
    ),
    url(
        r'^api/organizations/(?P<pk>\d+)$',
        OrganizationDetailAPIView.as_view(),
        name='api-organizations-detail',
    ),
    url(
        r'^api/organizations/(?P<pk>\d+)/carpoolers$',
        OrganizationCarPoolerAPIView.as_view(),
        name='api-organization-carpooler-detail',
    ),
    url(
        r'^api/carpoolers$',
        CarPoolerListAPIView.as_view(),
        name='api-carpoolers',
    ),
    url(
        r'^api/carpoolers/(?P<pk>\d+)$',
        CarPoolerDetailAPIView.as_view(),
        name='api-carpoolers-detail',
    ),
)

from rest_framework import serializers

from apps.organizations.models import Organization, CarPooler


class OrganizationSerializer(serializers.ModelSerializer):
    carpoolers = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Organization


class CarPoolerSerializer(serializers.ModelSerializer):
    departure_time = serializers.TimeField(format="%I:%M %p")
    arrival_time = serializers.TimeField(format="%I:%M %p")

    class Meta:
        model = CarPooler

from django.views.generic import TemplateView, CreateView
from rest_framework import generics

from .models import Organization, CarPooler
from .serializers import OrganizationSerializer, CarPoolerSerializer


class MapView(TemplateView):
    template_name = "organizations/map.html"

class AddOrgView(CreateView):
    model = Organization
    template_name = "organizations/organization.html"
    success_url = "/organizations"

class AddCarView(CreateView):
    model = CarPooler
    template_name = "organizations/carpoolers.html"
    success_url = "/organizations"

class OrganizationListAPIView(generics.ListCreateAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer


class OrganizationDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    model = Organization
    serializer_class = OrganizationSerializer


class CarPoolerListAPIView(generics.ListCreateAPIView):
    queryset = CarPooler.objects.all()
    serializer_class = CarPoolerSerializer


class CarPoolerDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    model = CarPooler
    serializer_class = CarPoolerSerializer


class OrganizationCarPoolerAPIView(generics.ListAPIView):
    serializer_class = CarPoolerSerializer

    def get_queryset(self):
        return Organization.objects.get(pk=self.kwargs['pk']).carpoolers.all()

# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Organization'
        db.create_table(u'organizations_organization', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('location', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'organizations', ['Organization'])

        # Adding model 'CarPooler'
        db.create_table(u'organizations_carpooler', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=254)),
            ('departure_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('car_description', self.gf('django.db.models.fields.TextField')()),
            ('leaving_from', self.gf('django.db.models.fields.TextField')()),
            ('going_to', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'organizations', ['CarPooler'])


    def backwards(self, orm):
        # Deleting model 'Organization'
        db.delete_table(u'organizations_organization')

        # Deleting model 'CarPooler'
        db.delete_table(u'organizations_carpooler')


    models = {
        u'organizations.carpooler': {
            'Meta': {'object_name': 'CarPooler'},
            'car_description': ('django.db.models.fields.TextField', [], {}),
            'departure_time': ('django.db.models.fields.DateTimeField', [], {}),
            'going_to': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'leaving_from': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'organizations.organization': {
            'Meta': {'object_name': 'Organization'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        }
    }

    complete_apps = ['organizations']
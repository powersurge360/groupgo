# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Organization.description'
        db.add_column(u'organizations_organization', 'description',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)

        # Adding field 'Organization.latitude'
        db.add_column(u'organizations_organization', 'latitude',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Organization.longitude'
        db.add_column(u'organizations_organization', 'longitude',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Organization.description'
        db.delete_column(u'organizations_organization', 'description')

        # Deleting field 'Organization.latitude'
        db.delete_column(u'organizations_organization', 'latitude')

        # Deleting field 'Organization.longitude'
        db.delete_column(u'organizations_organization', 'longitude')


    models = {
        u'organizations.carpooler': {
            'Meta': {'object_name': 'CarPooler'},
            'car_description': ('django.db.models.fields.TextField', [], {}),
            'departure_time': ('django.db.models.fields.DateTimeField', [], {}),
            'going_to': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'leaving_from': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        },
        u'organizations.organization': {
            'Meta': {'object_name': 'Organization'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.TextField', [], {}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '254'})
        }
    }

    complete_apps = ['organizations']
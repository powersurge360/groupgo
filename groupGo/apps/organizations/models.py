from django.db import models
import requests
import simplejson as json

GEOCODING_URL = "http://maps.googleapis.com/maps/api/geocode/json"


class Organization(models.Model):
    name = models.CharField(max_length=254)
    location = models.TextField()
    description = models.TextField()
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.latitude and not self.longitude:
            geocoded_blob = json.loads(requests.get(
                GEOCODING_URL,
                params={
                    "address": self.location,
                    "sensor": 'false'
                }
            ).content)

            location = geocoded_blob['results'][0]['geometry']['location']
            self.latitude = location['lat']
            self.longitude = location['lng']
        super(Organization, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class CarPooler(models.Model):
    name = models.CharField(max_length=254)
    arrival_time = models.TimeField()
    departure_time = models.TimeField()
    car_description = models.TextField()
    leaving_from = models.TextField()
    going_to = models.TextField()
    organization = models.ForeignKey(Organization, related_name="carpoolers")
    departure_latitude = models.FloatField(blank=True, null=True)
    departure_longitude = models.FloatField(blank=True, null=True)
    arrival_latitude = models.FloatField(blank=True, null=True)
    arrival_longitude = models.FloatField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.arrival_latitude and not self.arrival_longitude:
            geocoded_blob = json.loads(requests.get(
                GEOCODING_URL,
                params={
                    "address": self.going_to,
                    "sensor": 'false'
                }
            ).content)

            location = geocoded_blob['results'][0]['geometry']['location']
            self.arrival_latitude = location['lat']
            self.arrival_longitude = location['lng']

        if not self.departure_latitude and not self.departure_longitude:
            geocoded_blob = json.loads(requests.get(
                GEOCODING_URL,
                params={
                    "address": self.leaving_from,
                    "sensor": 'false'
                }
            ).content)

            location = geocoded_blob['results'][0]['geometry']['location']
            self.departure_latitude = location['lat']
            self.departure_longitude = location['lng']
        super(CarPooler, self).save(*args, **kwargs)

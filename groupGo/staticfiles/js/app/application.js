requirejs.config({
    "baseUrl": "/static/js/bower_components",
    "paths": {
        "app": "../app",
        "backbone": "backbone/backbone-min",
        "jquery": "jquery/jquery.min",
        "underscore": "underscore/underscore-min",
        "text": "requirejs-text/text",
        "async": "requirejs-plugins/src/async"
    },
    "shim": {
        "underscore": {
            "exports": "_"
        },
        "backbone": {
            "deps": ["underscore", "jquery"],
            "exports": "Backbone"
        }
    }
});

require(["backbone", "jquery", "app/router"], function(Backbone, $, MapRouter) {
    $(function() {
        var router = new MapRouter();
        Backbone.history.start();
    });
});

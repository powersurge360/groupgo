define([
        "app/vendor/gmaps",
        "backbone",
        "jquery",
        "underscore",
        "app/models/organization",
        "app/models/carpooler",
        "app/views"
    ], function(gmaps, Backbone, $, _, organization, carpool, views) {
    "use strict";

    var MapRouter = Backbone.Router.extend({
        "mapIsSetup": false,
        "organizationPins": [],
        "carPoolArrivingPins": [],
        "carPoolDepartingPins": [],
        "organizationCollection": new organization.OrganizationCollection(),

        "routes": {
            "": "index",
            "index": "index",
            "organization/:id": "organization",
            "car/arriving/:id": "car_arriving",
            "car/departing/:id": "car_departing"
        },

        // Route Handlers
        "index": function() {
            this.changeHandlers();
            var self = this;
            this.organizationCollection.fetch().success(function() {
                self.createOrganizationPins();
            });
        },

        "organization": function(id) {
            this.changeHandlers();
        },

        "car_arriving": function(id) {
            this.changeHandlers();

            var collection = new carpool.CarpoolerCollection(),
                self = this
            ;
            collection.orgID = id;
            collection.fetch().success(function(elements) {
                self.createCarpoolerArrivingPins(collection);
            });
        },

        "car_departing": function(id) {
            this.changeHandlers();
            var self = this;
            var collection = new carpool.CarpoolerCollection();
            collection.orgID = id;
            collection.fetch().success(function(elements) {
                self.createCarpoolerDeparturePins(collection);
            });
        },

        // Utility functions
        "changeHandlers": function() {
            if (this.organizationPins.length > 0) {
                _.each(this.organizationPins, function(pin) {
                    pin.destroy();
                });

                this.organizationPins = [];
            }

            if (this.carPoolArrivingPins.length > 0) {
                _.each(this.carPoolArrivingPins, function(pin) {
                    pin.destroy();
                });
            }

            if (this.carPoolDepartingPins.length > 0) {
                _.each(this.carPoolDepartingPins, function(pin) {
                    pin.destroy();
                });
            }

            this.setUpMap();
        },

        "createOrganizationPins": function() {
            var self = this;
            var bounds = new gmaps.LatLngBounds();
            this.organizationCollection.each(function(element) {
                var pin = new views.OrganizationPin({
                    "model": element
                });

                if (pin.assignMap(self.map).render() === true) {
                    self.organizationPins.push(pin);
                    bounds.extend(pin.latLng);
                    pin.map.fitBounds(bounds);
                }
            });
        },

        "createCarpoolerArrivingPins": function(collection) {
            var self = this;
            var bounds = new gmaps.LatLngBounds();
            collection.each(function(element) {
                var pin = new views.CarpoolerArrivingPin({
                    "model": element
                });

                if (pin.assignMap(self.map).render() === true) {
                    self.carPoolArrivingPins.push(pin);
                    bounds.extend(pin.latLng);
                    pin.map.fitBounds(bounds);
                }
            });
        },

        "createCarpoolerDeparturePins": function(collection) {
            var self = this;
            var bounds = new gmaps.LatLngBounds();
            collection.each(function(element) {
                var pin = new views.CarpoolerDepartingPin({
                    "model": element
                });

                if (pin.assignMap(self.map).render() === true) {
                    self.carPoolDepartingPins.push(pin);
                    bounds.extend(pin.latLng);
                    pin.map.fitBounds(bounds);
                }
            });
        },

        "setUpMap": function() {
            if (this.mapIsSetup === true) {
                return;
            }

            var styles = [
                {
                  stylers: [
                    { hue: "#00ffe6" },
                    { saturation: -20 }
                  ]
                },{
                  featureType: "road",
                  elementType: "geometry",
                  stylers: [
                    { lightness: 100 },
                    { visibility: "simplified" }
                  ]
                },{
                  featureType: "road",
                  elementType: "labels",
                  stylers: [
                    { visibility: "off" }
                  ]
                },{
                    "featureType": "road",
                    "stylers": [
                      { "visibility": "on" },
                      { "invert_lightness": true },
                      { "hue": "#3bff00" },
                      { "lightness": 80 }
                    ]
                }
              ];

            // Create a new StyledMapType object, passing it the array of styles,
            // as well as the name to be displayed on the map type control.
            var styledMap = new google.maps.StyledMapType(styles,
            {name: "Styled Map"});

            var mapOptions = {
                "zoom": 10,
                "center": new gmaps.LatLng(38.217953, -85.748291),
                "mapTypeId": gmaps.MapTypeId.ROADMAP,
                "zoomControl": false,
                "streetViewControl": false,
                "panControl": false,
                "mapTypeControl": false,
                "styles": styles
            };

            this.map = new gmaps.Map($("#map-canvas")[0], mapOptions);

            //Associate the styled map with the MapTypeId and set it to display.
            this.map.mapTypes.set('map_style', styledMap);
            this.map.setMapTypeId('map_style');
        }
    });

    return MapRouter;
});

define([
    "app/vendor/gmaps",
    "backbone",
    "underscore",
    "text!app/templates/organization_pin.html",
    "text!app/templates/carpool_pin.html"
], function(gmaps, Backbone, _, organizationTemplate, carpoolTemplate) {
    "use strict";

    var OrganizationPin = Backbone.View.extend({
        "template": _.template(organizationTemplate),

        "assignMap": function(map) {
            this.map = map;

            return this;
        },

        "render": function() {
            if (this.model.get("latitude") && this.model.get("longitude")) {
                this.latLng = new gmaps.LatLng(this.model.get("latitude"), this.model.get("longitude"));
                this.pin = new gmaps.Marker({
                    "position": this.latLng,
                    "map": this.map
                });

                var infoWindow = new gmaps.InfoWindow({
                    "content": this.template(this.model.toJSON())
                });

                var self = this;
                gmaps.event.addListener(this.pin, "click", function() {
                    infoWindow.open(self.map, self.pin);
                });
                return true;
            }

            return false;
        },

        "destroy": function() {
            this.pin.setMap(null);
        }
    });

    var CarpoolerArrivingPin = Backbone.View.extend({
        "template": _.template(carpoolTemplate),

        "assignMap": function(map) {
            this.map = map;

            return this;
        },

        "render": function() {
            if (this.model.get("arrival_latitude") && this.model.get("arrival_longitude")) {

                this.latLng = new gmaps.LatLng(
                    this.model.get("arrival_latitude"),
                    this.model.get("arrival_longitude")
                );

                this.pin = new gmaps.Marker({
                    "position": this.latLng,
                    "map": this.map,
                    "icon": "/static/img/carpooler-pin.png"
                });

                var infoWindow = new gmaps.InfoWindow({
                    "content": this.template(this.model.toJSON())
                });

                var self = this;
                gmaps.event.addListener(this.pin, "click", function() {
                    infoWindow.open(self.map, self.pin);
                });
                return true;
            }

            return false;
        },

        "destroy": function() {
            this.pin.setMap(null);
        }
    });

    var CarpoolerDepartingPin = Backbone.View.extend({
        "template": _.template(carpoolTemplate),

        "assignMap": function(map) {
            this.map = map;

            return this;
        },

        "render": function() {
            if (this.model.get("departure_latitude") && this.model.get("departure_longitude")) {
                this.latLng = new gmaps.LatLng(
                    this.model.get("departure_latitude"),
                    this.model.get("departure_longitude")
                );

                this.pin = new gmaps.Marker({
                    "position": this.latLng,
                    "map": this.map,
                    "icon": "/static/img/carpooler-pin.png"
                });

                var infoWindow = new gmaps.InfoWindow({
                    "content": this.template(this.model.toJSON())
                });

                var self = this;
                gmaps.event.addListener(this.pin, "click", function() {
                    infoWindow.open(self.map, self.pin);
                });
                return true;
            }

            return false;
        },

        "destroy": function() {
            this.pin.setMap(null);
        }
    });

    return {
        "OrganizationPin": OrganizationPin,
        "CarpoolerArrivingPin": CarpoolerArrivingPin,
        "CarpoolerDepartingPin": CarpoolerDepartingPin
    };
});

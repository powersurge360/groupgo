define(["backbone"], function(Backbone) {
    "use strict";

    var Carpooler = Backbone.Model.extend();

    var CarpoolerCollection = Backbone.Collection.extend({
        "model": Carpooler,

        "url":  function() {
            return "/organizations/api/organizations/"+this.orgID+"/carpoolers";
        }
    });

    return {
        "CarpoolerCollection": CarpoolerCollection,
        "Carpooler": Carpooler
    };
});

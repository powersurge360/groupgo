define(["backbone"], function(Backbone) {
    "use strict";

    var Organization = Backbone.Model.extend();

    var OrganizationCollection = Backbone.Collection.extend({
        "url": "/organizations/api/organizations",
        "model": Organization
    });

    return {
        "OrganizationCollection": OrganizationCollection,
        "Organization": Organization
    };
});

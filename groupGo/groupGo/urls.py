from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    '',
    url(r'^organizations/', include('apps.organizations.urls')),
    url(r'^organizations/add/', include('apps.organizations.urls'))
)
